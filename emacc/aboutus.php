</head>
<body>
	<header id="main-header">
		<div class="container">
			<h1>OSU HIRING OPPORTUNITIES</h1>
		</div>
	</header>

	<nav id="navbar">
		<div class="container">
			<ul>
				<li><a href="home.php">Home</a></li>
			<li><a href="EmAccount.php">My Account</a></li>
       <li><a href="contactus.php">Contact Us </a></li>
			<li><a href="logout.php">Log out</a></li>
			<br>
				<?php

include_once('Emindex.php');
include_once('link.php');
           ?> 
		</ul>
		</div>
	</nav>

	<div class="container">
		<section id="main">
			<h1>About Us</h1>


			<p>We are students at Oregon State University.</p>
		 	<p>
		 		The goal of our project is to prove the ability to improve project success rates by using an Agile methodology. Agile project management is an approach to complete the work efficiently and manage a project by breaking down a large project into more manageable tasks to be completed in short iterations throughout sprints. Our project idea will allow the students to search and find jobs. Also, it will give employers the ability to post new jobs or internships for students in their field on campus, or can help students to have connections with different employers that could land a job later for them.</p>
		 			<p> Our project idea will solve the lack of communication and gaining experience between students and employers. We used HTML, CSS, and PHP to program this project. We faced some challenges in designing different page functionalities for students and employers. However, we learned many things, such as test-driven development by writing the testing and verify if it is a success or failure. We think that testing updates are valuable because we could check the tests into source code control and get help from other members daily as we shared our testing and code updates. Our future vision is to improve the communication and messaging between employers and students as new functionality of our project. </p>
		</section>
		

	</div>

	<footer id="main-footer">
		<p>Copyright &copy; 2020, OSU-CS561 Class Students</p>
	</footer>
</body>
</html>